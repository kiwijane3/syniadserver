import XCTest
@testable import SyniadServer

final class SyniadServerTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SyniadServer().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
