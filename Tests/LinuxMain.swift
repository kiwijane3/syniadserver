import XCTest

import SyniadServerTests

var tests = [XCTestCaseEntry]()
tests += SyniadServerTests.allTests()
XCTMain(tests)
