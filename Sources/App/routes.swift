import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
	
	let accountController = AccountController();
	accountController.register(to: router);
	
	let dataController = DataController();
	dataController.register(to: router);
	
	let profileController = ProfileController();
	profileController.register(to: router);
	
	let itemController = ItemController();
	itemController.register(to: router);
	
	let reviewController = ReviewController();
	reviewController.register(to: router);
	
	let projectController = ProjectController();
	projectController.register(to: router);
	
	let feedController = FeedController();
	feedController.register(to: router);
}
