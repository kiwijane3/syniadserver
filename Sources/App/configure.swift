import Vapor
import MongoKitten

extension MongoKitten.Database: Service {}

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {

	debugPrint("Configure");
	
    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    // middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

	let databaseURI = "mongodb://localhost/syniad";
	
	services.register { container -> MongoKitten.Database in
		return try! Database.lazyConnect(databaseURI, on: container.eventLoop);
	}

	services.register { container -> JwtService in
		return try! JwtService();
	}
	
	services.register() { container -> LoginAccessor in
		return try! LoginAccessor(database: container.make(MongoKitten.Database.self));
	}
	
	services.register() { container -> ProfileAccessor in
		return try! ProfileAccessor(database: container.make(MongoKitten.Database.self));
	}
	
	services.register() { container -> DataAccessor in
		return try! DataAccessor(database: container.make(MongoKitten.Database.self));
	}
	
	services.register() { container -> ItemAccessor in
		return try! ItemAccessor(database: container.make(MongoKitten.Database.self));
	}
	
	services.register() { container -> ReviewAccessor in
		return try! ReviewAccessor(database: container.make(MongoKitten.Database.self));
	}
	
	services.register() { container -> ProjectAccessor in
		return try! ProjectAccessor(database: container.make(MongoKitten.Database.self));
	}
	
	services.register() { container -> FeedAccessor in
		return try! FeedAccessor(database: container.make(MongoKitten.Database.self));
	}
	
}
