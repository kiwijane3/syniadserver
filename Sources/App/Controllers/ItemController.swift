//
//  ItemController.swift
//  App
//
//  Created by Jane Fraser on 21/01/20.
//

import Foundation
import Vapor
import SyniadShared

public class ItemController {
	
	public func register(to router: Router) {
		router.get("items", use: itemSearch);
		router.get("items", String.parameter, use: getItem);
		router.post("items", use: postItem);
		router.patch("items", String.parameter, use: updateItem);
		router.post("items", String.parameter, "content", use: postItemContent);
		router.post("items", String.parameter, "images", use: postImagesToItem);
		router.delete("items", String.parameter, "images", use: deleteImagesFromItem);
		router.patch("items", String.parameter, "images", "order", use: reorderImage);
	}
	
	public func itemSearch(_ req: Request) throws -> Future<[Item]> {
		let term = req.query[String.self, at: "term"];
		let category = req.query[Category.self, at: "category"];
		let style = req.query[String.self, at: "style"];
		let tags = req.query[[String].self, at: "tags"];
		let publisher = req.query[ID.self, at: "publisher"];
		let skip = req.query[Int.self, at: "skip"];
		return try req.itemAccessor().items(for: term, in: category, withStyle: style, tagged: tags, by: publisher, skip: skip);
	}
	
	public func getItem(_ req: Request) throws -> Future<Item> {
		let id = try req.parameters.next(String.self);
		return try req.itemAccessor().item(forID: id);
	}
	
	// Returns the ID of the inserted item.
	public func postItem(_ req: Request) throws -> Future<ID> {
		return try req.content.decode(ItemCreationRequest.self)
			.flatMap{ creationRequest -> Future<Item> in
				// Check that the user has authorisation to access the specified publisher.
				guard try req.jwtService().verifyAuthorisation(for: req, toProfile: creationRequest.publisher) else {
					throw AccessError.unauthorised;
				}
				guard creationRequest.valid else {
					throw ResourceError.invalid;
				}
				let item = Item(byUser: creationRequest.publisher,
								inCategory: creationRequest.category!,
								style: creationRequest.style!,
								withTags: creationRequest.tags,
								titled: creationRequest.title,
								description: creationRequest.description,
								contentType: creationRequest.contentType!);
				return try req.itemAccessor().insert(item: item);
			}.flatMap { item in
				return try req.feedAccessor().insert(post: Post(from: item.publisherID, type: .creation, item: item)).and(result: item);
			}.map({ (result) in
				let (_, item) = result;
				return item.id;
			});
	}
	
	public func updateItem(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		return try req.content.decode(ItemUpdateRequest.self)
			.and(req.itemAccessor().item(forID: target))
			.map{ (result) -> Future<Item> in
				let (updateRequest, item) = result;
				guard try req.jwtService().verifyAuthorisation(for: req, toProfile: item.publisherID) else {
					throw AccessError.unauthorised;
				}
				if let category = updateRequest.newCategory {
					item.category = category;
				}
				if let style = updateRequest.newStyle {
					item.style = style;
				}
				if let title = updateRequest.newTitle {
					item.title = title;
				}
				if let description = updateRequest.newDescription {
					item.description = description;
				}
				if !updateRequest.addTags.isEmpty {
					item.tags.append(contentsOf: updateRequest.addTags);
				}
				if !updateRequest.removeTags.isEmpty {
					item.tags.removeAll { (element) -> Bool in
						updateRequest.removeTags.contains(element);
					}
				}
				return try req.itemAccessor().insert(item: item);
			}.map { (item) in
				return VoidResponse();
			}
	}
	
	public func postItemContent(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		return try req.content.decode(DataRequest.self)
			.and(req.itemAccessor().item(forID: target))
			.flatMap({ (result) -> Future<(ID, Item)> in
				let (dataRequest, item) = result;
				guard try req.jwtService().verifyAuthorisation(for: req, toProfile: item.publisherID) else {
					throw AccessError.unauthorised;
				}
				guard dataRequest.dataType == dataType(for: item.contentType) else {
					throw ResourceError.incompatibleType;
				}
				return try req.dataAccessor().store(data: dataRequest.data, as: dataRequest.dataType).and(result: item);
			}).flatMap({ (result) -> Future<Item> in
				let (dataID, item) = result;
				item.update(dataID: dataID);
				return try req.itemAccessor().update(item: item);
			}).map({ (item) -> VoidResponse in
				return VoidResponse();
			})
	}
	
	public func postImagesToItem(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		// Begin by retrieving the item and validating the user's authorisation.
		return try req.itemAccessor().item(forID: target).flatMap({ (item) -> Future<([DataRequest], Item)> in
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: item.publisherID) else {
				throw AccessError.unauthorised;
			}
			// Now that we have validated the user, we can decode the request.
			return try req.content.decode([DataRequest].self).and(result: item);
		}).flatMap({ (result) -> Future<([ID], Item)> in
			var (dataRequests, item) = result;
			// Filter data requests for non-image entries.
			dataRequests = dataRequests.filter { (request) -> Bool in
				return request.dataType.isImage;
			}
			// If all requests were non-image, throw an error to break flow since we can't do anything.
			if dataRequests.isEmpty {
				throw ResourceError.incompatibleType;
			}
			// Insert data into database and retrieve IDs
			return try req.dataAccessor().store(contentsOf: dataRequests).and(result: item);
		}).flatMap({ (result) -> Future<Item> in
			let (imageIDs, item) = result;
			// Add ids to the item and update it.
			item.images.append(contentsOf: imageIDs);
			return try req.itemAccessor().update(item: item);
		}).map({ (item) in
			return VoidResponse();
		})
	}
	
	public func deleteImagesFromItem(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		// Check the user has permission to access this item
		return try req.itemAccessor().item(forID: target).flatMap({ (item) -> EventLoopFuture<([ID], Item)> in
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: item.publisherID) else {
				throw AccessError.unauthorised;
			}
			// Decode the request.
			return try req.content.decode([ID].self).and(result: item);
		}).flatMap({ (result) -> EventLoopFuture<Item> in
			let (removalIDs, item) = result;
			item.images.removeAll { (imageID) -> Bool in
				return removalIDs.contains(imageID);
			}
			return try req.itemAccessor().update(item: item);
		}).map { item in
			return VoidResponse();
		}
	}
	
	public func reorderImage(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		return try req.itemAccessor().item(forID: target).flatMap { (item) -> EventLoopFuture<(ImagePriorities, Item)> in
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: item.publisherID) else {
				throw AccessError.unauthorised;
			} 
			return try req.content.decode(ImagePriorities.self).and(result: item);
		}.flatMap { (result) -> EventLoopFuture<Item> in
			let (priorities, item) = result;
			item.images.sort(by: { (a, b) in
				// Lower priority value elements are placed before higher priority value elements; Lower value indiciates greater priority.
				let aPriority = priorities[a] ?? Int.max;
				let bPriority = priorities[b] ?? Int.max;
				return aPriority < b.priority
			});
			return try req.itemAccessor().update(item: item);
		}.map { (item) -> VoidResponse in
			return VoidResponse();
		}
	}
}

func dataType(for contentType: ContentType) -> DataType {
	switch contentType {
	case .sewingPattern:
		return .sewingPattern;
	}
}
