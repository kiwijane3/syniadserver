//
//  ReviewController.swift
//  App
//
//  Created by Jane Fraser on 23/01/20.
//

import Foundation
import Vapor
import SyniadShared

public class ReviewController {
	
	public func register(to router: Router) {
		router.get("items", String.parameter, "reviews", use: getItemReviews);
		router.get("reviews", String.parameter, use: getReview);
		router.get("items", String.parameter, "reviews", "user", String.parameter, use: getReviewForItemAndUser);
		router.post("items", String.parameter, "reviews", use: postReview);
		router.patch("reviews", String.parameter, use: patchReview);
		router.post("reviews", String.parameter, "images", use: postImagesToReview);
		router.delete("reviews", String.parameter, "images", use: deleteImagesFromReview);
	}
	
	public func getReview(_ req: Request) throws  -> Future<ItemReview> {
		let id = try req.parameters.next(String.self);
		return try req.reviewAccessor().review(forID: id);
	}

	public func getReviewForItemAndUser(_ req: Request) throws -> Future<ItemReview> {
		let itemID = try req.parameters.next(String.self);
		let userID = try req.parameters.next(String.self);
		return try req.reviewAccessor().review(fromUser: userID, onItem: itemID);
	}
	
	public func getItemReviews(_ req: Request) throws -> Future<[ItemReview]> {
		let itemID = try req.parameters.next(String.self);
		let skip = req.query[Int.self, at: "skip"];
		return try req.reviewAccessor().reviews(forItem: itemID, skip: skip);
	}
	
	public func postReview(_ req: Request) throws -> Future<String> {
		let target = try req.parameters.next(String.self);
		return try req.content.decode(ItemReviewRequest.self)
			.flatMap({ (reviewRequest) -> Future<(ItemReview, Item)> in
				// Verify the user has the correct authorisation.
				guard try req.jwtService().verifyAuthorisation(for: req, toProfile: reviewRequest.userID) else {
					throw AccessError.unauthorised;
				}
				// Create and insert the review
				let review = ItemReview(ofItem: target, by: reviewRequest.userID, endorsed: reviewRequest.endorsed, description: reviewRequest.description);
				return try req.reviewAccessor().insert(review: review).and(req.itemAccessor().item(forID: target));
			}).flatMap({ (result) -> Future<(Item, ItemReview)> in
				// Now that we have created the review, modify the item's endorsement counts.
				let (review, item) = result;
				if review.endorsed {
					item.incrementEndorsementCount();
				} else {
					item.incrementNonEndorsementCount();
				}
				return try req.itemAccessor().update(item: item).and(result: review);
			}).flatMap { result -> Future<(Post, ItemReview)> in
				let (item, review) = result;
				if review.endorsed {
					return try req.feedAccessor().insert(post: Post(from: review.userID, type: .endorsed, item: item)).and(result: review);
				} else {
					// Return a dummy post that isn't inserted.
					return req.future(Post(from: review.userID, type: .endorsed, item: item)).and(result: review);
				}
			}.map({ (result) -> String in
				let (_, review) = result;
				return review.id;
			})
	}
	
	public func patchReview(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		return try req.reviewAccessor().review(forID: target).flatMap({ (review) -> EventLoopFuture<(ItemReviewUpdateRequest, ItemReview)> in
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: review.userID) else {
				throw AccessError.unauthorised;
			}
			return try req.content.decode(ItemReviewUpdateRequest.self).and(result: review);
		}).flatMap({ (result) -> EventLoopFuture<(ItemReview, (toggled: Bool, value: Bool))> in
			let (updateRequest, review) = result;
			// Record whether the endorsement was toggled, and to what value.
			var toggled: (toggled: Bool, value: Bool);
			if let endorsed = updateRequest.endorsed {
				toggled = (toggled: endorsed == review.endorsed, value: endorsed);
				review.endorsed = endorsed;
			} else {
				toggled = (toggled: false, value: review.endorsed);
			}
			if let description = updateRequest.description {
				review.description = description;
			}
			// Update the review and forward the toggle status.
			return try req.reviewAccessor().update(review: review).and(result: toggled);
		}).flatMap({ (result) -> EventLoopFuture<(Item, (toggled: Bool, value: Bool))> in
			let (review, toggled) = result;
			return try req.itemAccessor().item(forID: review.itemID).and(result: toggled);
		}).flatMap({ (result) -> EventLoopFuture<Item> in
			let (item, toggled) = result;
			if toggled.toggled {
				if toggled.value {
					item.decrementNonEndorsementCount();
					item.incrementEndorsementCount();
				} else {
					item.decrementNonEndorsementCount();
					item.incrementNonEndorsementCount();
				}
				return try req.itemAccessor().update(item: item);
			} else {
				return req.future(item);
			}
		}).map({ (item) -> VoidResponse in
			return VoidResponse();
		})
	}
	
	public func postImagesToReview(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		return try req.reviewAccessor().review(forID: target).flatMap({ (review) -> Future<([DataRequest], ItemReview)> in
			// Check the user is authorised to modify this review.
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: review.userID) else {
				throw AccessError.unauthorised;
			}
			return try req.content.decode([DataRequest].self).and(result: review);
		}).flatMap({ (result) -> Future<([ID], ItemReview)> in
			var (dataRequests, review) = result;
			dataRequests = dataRequests.filter({ (request) -> Bool in
				return request.dataType.isImage;
			});
			if dataRequests.isEmpty {
				throw ResourceError.incompatibleType;
			}
			return try req.dataAccessor().store(contentsOf: dataRequests).and(result: review);
		}).flatMap({ (result) -> EventLoopFuture<ItemReview> in
			let (imageIDs, review) = result;
			review.images.append(contentsOf: imageIDs);
			return try req.reviewAccessor().update(review: review);
		}).map({ (review) in
			return VoidResponse();
		})
	}
	
	public func deleteImagesFromReview(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		return try req.reviewAccessor().review(forID: target).flatMap({ (review) -> EventLoopFuture<([ID], ItemReview)> in
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: review.userID) else {
				throw AccessError.unauthorised;
			}
			return try req.content.decode([ID].self).and(result: review)
		}).flatMap({ (result) -> EventLoopFuture<ItemReview> in
			let (removalIDs, review) = result;
			review.images.removeAll { (imageID) -> Bool in
				removalIDs.contains(imageID);
			}
			return try req.reviewAccessor().update(review: review);
		}).map({ (review) in
			return VoidResponse();
		})
	}
	
}
