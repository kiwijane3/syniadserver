//
//  FeedAccessor.swift
//  App
//
//  Created by Jane Fraser on 29/01/20.
//

import Foundation
import Vapor
import SyniadShared

public class FeedController {
	
	public func register(to router: Router) {
		router.post("profile", String.parameter, "share", use: shareContent);
		router.get("profile", String.parameter, "posts", use: getPostsFromUser);
		router.get("profile", String.parameter, "feed", use: getFeed);
	}
	
	public func shareContent(_ req: Request) throws -> Future<VoidResponse> {
		let userID = try req.parameters.next(String.self);
		// We don't need to retrieve the profile to verify authorisation, since we already have the ID.
		guard try req.jwtService().verifyAuthorisation(for: req, toProfile: userID) else {
			throw AccessError.unauthorised;
		}
		return try req.content.decode(ShareRequest.self).flatMap({ (shareRequest) -> Future<Post> in
			let post = Post(from: userID, withType: .shared, contentType: shareRequest.contentType, contentID: shareRequest.contentID, postIndex: shareRequest.postIndex);
			return try req.feedAccessor().insert(post: post);
			}).map { result in
				return VoidResponse();
			}
	}
	
	public func getPostsFromUser(_ req: Request) throws -> Future<[Post]> {
		let userID = try req.parameters.next(String.self);
		let skip = req.query[Int.self, at: "skip"];
		return try req.feedAccessor().posts(fromUser: userID, skip: skip);
	}
	
	public func getFeed(_ req: Request) throws -> Future<[Post]> {
		let userID = try req.parameters.next(String.self);
		let skip = req.query[Int.self];
		guard try req.jwtService().verifyAuthorisation(for: req, toProfile: userID) else {
			throw AccessError.unauthorised;
		}
		return try req.profileAccessor().userProfile(forID: userID).flatMap({ (profile) in
			return try req.feedAccessor().posts(fromUsers: profile.subscriptions, skip: skip);
		})
	}
}
