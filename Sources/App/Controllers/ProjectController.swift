//
//  ProjectController.swift
//  App
//
//  Created by Jane Fraser on 27/01/20.
//

import Foundation
import Vapor
import SyniadShared

public class ProjectController {
	
	public func register(to router: Router) {
		router.get("projects", String.parameter, use: getProject);
		router.get("items", String.parameter, "projects", use: getProjectsForItem)
		router.get("profile", String.parameter, "projects", use: getProjectsForUser);
		router.post("profile", String.parameter, "projects", use: createProject);
		router.post("projects", String.parameter, "posts", use: createPost);
		router.patch("projects", String.parameter, "posts", Int.parameter, use: editPost);
		router.post("projects", String.parameter, "posts", Int.parameter, "images", use: addImagesToPost);
		router.delete("projects", String.parameter, "posts", Int.parameter, "images", use: removeImagesFromPost);
	}
	
	public func getProject(_ req: Request) throws -> Future<Project>{
		let projectID = try req.parameters.next(String.self);
		return try req.projectAccessor().project(forID: projectID);
	}
	
	public func getProjectsForItem(_ req: Request) throws -> Future<[Project]> {
		let itemID = try req.parameters.next(String.self);
		let skip = try req.query[Int.self, at: "skip"];
		return try req.projectAccessor().projects(forItem: itemID, skip: skip);
	}
	
	public func getProjectsForUser(_ req: Request) throws -> Future<[Project]> {
		let userID = try req.parameters.next(String.self);
		let skip = try req.query[Int.self, at: "skip"];
		return try req.projectAccessor().projects(from: userID, skip: skip);
	}
	
	public func createProject(_ req: Request) throws -> Future<String> {
		let userID = try req.parameters.next(String.self);
		// We only retrieve the userProfile to check it exists, so we don't create orphaned projects.
		return try req.profileAccessor().userProfile(forID: userID).flatMap({ (userProfile) -> Future<ID> in
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: userProfile._id) else {
				throw AccessError.unauthorised;
			}
			// The content should just specify the item, so is just an id.
			return try req.content.decode(ID.self);
		}).flatMap { (itemID) -> Future<Item> in
			// Retrieve the item to ensure the item exists.
			return try req.itemAccessor().item(forID: itemID);
		}.flatMap({ (item) -> Future<Project> in
			let project = Project(by: userID, for: item.id);
			return try req.projectAccessor().insert(project: project);
		}).map({ (project) -> ID in
			return project._id;
		})
	}
	
	// Returns the ordinal position of the created post, starting at 0
	public func createPost(_ req: Request) throws -> Future<Int> {
		let projectID = try req.parameters.next(String.self);
		return try req.projectAccessor().project(forID: projectID).flatMap({ (project) -> Future<(ProjectPostRequest, Project)> in
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: project.user) else {
				throw AccessError.unauthorised;
			}
			return try req.content.decode(ProjectPostRequest.self).and(result: project);
		}).flatMap({ (result) -> EventLoopFuture<Project> in
			let (postRequest, project) = result;
			project.update(title: postRequest.title, body: postRequest.body, complete: postRequest.complete);
			return try req.projectAccessor().update(project: project);
		}).flatMap({ (project) in
			return try req.feedAccessor().insert(post: Post(from: project.user, type: .creation, post: project.posts.count - 1, on: project)).and(result: project);
		}).map({ (result) in
			let (_, project) = result;
			return project.posts.count - 1;
		})
	}
	
	public func editPost(_ req: Request) throws -> Future<VoidResponse> {
		let projectID = try req.parameters.next(String.self);
		let postIndex = try req.parameters.next(Int.self);
		return try req.projectAccessor().project(forID: projectID).flatMap({ (project) -> Future<(ProjectPostUpdateRequest, Project)> in
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: project.user) else {
				throw AccessError.unauthorised;
			}
			// Decode the request and forward it to the next phase with the project.
			return try req.content.decode(ProjectPostUpdateRequest.self).and(result: project);
		}).flatMap({ (result) -> Future<Project> in
			let (updateRequest, project) = result;
			if let title = updateRequest.title {
				project.posts[postIndex].title = title;
			}
			if let body = updateRequest.body {
				project.posts[postIndex].body = body;
			}
			return try req.projectAccessor().update(project: project);
		}).map { project in
			return VoidResponse();
		}
	}
	
	public func addImagesToPost(_ req: Request) throws -> Future<VoidResponse> {
		let project = try req.parameters.next(String.self);
		let postIndex = try req.parameters.next(Int.self);
		return try req.projectAccessor().project(forID: project).flatMap({ (project) -> Future<([DataRequest], Project)> in
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: project._id) else {
				throw AccessError.unauthorised;
			}
			return try req.content.decode([DataRequest].self).and(result: project);
		}).flatMap({ (result) -> Future<([ID], Project)> in
			var (dataRequests, project) = result;
			dataRequests.removeAll { (request) -> Bool in
				return !request.dataType.isImage;
			}
			if dataRequests.isEmpty {
				throw ResourceError.invalid;
			}
			return try req.dataAccessor().store(contentsOf: dataRequests).and(result: project);
		}).flatMap({ (result) -> Future<Project> in
			let (imageIDs, project) = result;
			project.addImages(imageIDs, at: postIndex);
			return try req.projectAccessor().update(project: project);
		}).map({ (project) in
			return VoidResponse();
		})
	}
	
	public func removeImagesFromPost(_ req: Request) throws -> Future<VoidResponse> {
		let project = try req.parameters.next(String.self);
		let postIndex = try req.parameters.next(Int.self);
		return try req.projectAccessor().project(forID: project).flatMap({ (project) -> Future<([ID], Project)> in
			guard try req.jwtService().verifyAuthorisation(for: req, toProfile: project._id) else {
				throw AccessError.unauthorised;
			}
			return try req.content.decode([ID].self).and(result: project);
		}).flatMap({ (result) -> Future<Project> in
			let (imageIDs, project) = result;
			project.posts[postIndex].images.removeAll { imageID in
				return imageIDs.contains(imageID);
			}
			return try req.projectAccessor().update(project: project);
		}).map { project in
			return VoidResponse();
		}
	}
 
}
