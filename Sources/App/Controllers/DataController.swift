//
//  DataController.swift
//  App
//
//  Created by Jane Fraser on 24/01/20.
//

import Foundation
import Vapor
import SyniadShared

public class DataController {
	
	public func register(to router: Router) {
		router.get("data", String.parameter, use: getData);
	}
	
	public func getData(_ req: Request) throws -> Future<DataContainer> {
		let target = try req.parameters.next(String.self);
		return try req.dataAccessor().retrieve(target);
	}
	
}
