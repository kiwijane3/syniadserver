//
//  ProfileController.swift
//  App
//
//  Created by Jane Fraser on 20/01/20.
//

import Foundation
import Vapor
import SyniadShared

public class ProfileController {
	
	public func register(to router: Router) {
		router.get("profile", String.parameter, use: getProfile);
		router.patch("profile", String.parameter, use: patchProfile);
		router.post("profile", String.parameter, "avatar", use: postProfileAvatar);
		router.patch("profile", String.parameter, "subscriptions", use: editSubscriptions);
		router.patch("profile", String.parameter, "saved", use: editSavedItems);
	}
	
	public func getProfile(_ req: Request) throws -> Future<UserProfile> {
		let id = try req.parameters.next(String.self);
		return try req.profileAccessor().userProfile(forID: id);
	}
	
	public func patchProfile(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		// Verify the user is authorised to access the target.
		guard try req.jwtService().verifyAuthorisation(for: req, toProfile: target) else {
			throw AccessError.unauthorised;
		}
		return try req.content.decode(UserProfileUpdateRequest.self)
			.and(req.profileAccessor().userProfile(forID: target))
			.flatMap { result -> Future<UserProfile> in
				let (updateRequest, userProfile) = result;
				if let username = updateRequest.newUserName {
					userProfile.username = username;
				}
				if let description = updateRequest.newDescription {
					userProfile.description = description;
				}
				return try req.profileAccessor().update(profile: userProfile);
			}
			.map{ (profile) -> VoidResponse in
				return VoidResponse();
			}
	}
	
	public func postProfileAvatar(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		guard try req.jwtService().verifyAuthorisation(for: req, toProfile: target) else {
			throw AccessError.unauthorised;
		}
		return try req.content.decode(DataRequest.self)
			.and(try req.profileAccessor().userProfile(forID: target))
			.flatMap { (result) -> Future<(ID, UserProfile)> in
				let (dataRequest, userProfile) = result;
				guard dataRequest.dataType.isImage else {
					throw ResourceError.incompatibleType;
				}
				return try req.dataAccessor().store(data: dataRequest.data, as: dataRequest.dataType).and(result: userProfile);
		}.flatMap({ (result) -> Future<UserProfile> in
			let (dataID, userProfile) = result;
			userProfile.avatarID = dataID;
			return try req.profileAccessor().update(profile: userProfile);
		}).map({ (userProfile) in
			return VoidResponse();
		})
	}
	
	public func editSubscriptions(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		guard try req.jwtService().verifyAuthorisation(for: req, toProfile: target) else {
			throw AccessError.unauthorised;
		}
		return try req.content.decode(SubscriptionRequest.self)
			.and(try req.profileAccessor().userProfile(forID: target))
			.flatMap{ (result) -> Future<(UserProfile, (target: ID, delta: Int))> in
				let (subscriptionRequest, userProfile) = result;
				var modified = false;
				var followerModification: (target: ID, delta: Int) = (target: subscriptionRequest.element, delta: 0);
				switch subscriptionRequest.operation {
				case .insert:
					if !userProfile.subscriptions.contains(subscriptionRequest.element) {
						userProfile.subscriptions.append(subscriptionRequest.element);
						modified = true;
						followerModification.delta = 1;
					}
				case .remove:
					if let index = userProfile.subscriptions.index(of: subscriptionRequest.element) {
						userProfile.subscriptions.remove(at: index);
						modified = true;
						followerModification.delta = -1;
					}
				}
				if modified {
					return try req.profileAccessor().update(profile: userProfile).and(result: followerModification);
				} else {
					return req.future(userProfile).and(result: followerModification);
				}
			}.flatMap { (result) -> Future<(UserProfile, Int)> in
				let (_, (target, delta)) = result;
				return try req.profileAccessor().userProfile(forID: target).and(result: delta);
			}.flatMap { result -> Future<UserProfile> in
				let (targetProfile, delta) = result;
				if delta != 0 {
					if delta > 0 {
						targetProfile.incrementFollowers();
					} else if delta < 0 {
						targetProfile.decrementFollowers();
					}
					return try req.profileAccessor().update(profile: targetProfile);
				} else {
					return req.future(targetProfile);
				}
			}.map({ (userProfile) -> VoidResponse in
				return VoidResponse();
			});
	}
	
	public func editSavedItems(_ req: Request) throws -> Future<VoidResponse> {
		let target = try req.parameters.next(String.self);
		guard try req.jwtService().verifyAuthorisation(for: req, toProfile: target) else {
			throw AccessError.unauthorised;
		}
		return try req.content.decode(SaveRequest.self)
			.and(try req.profileAccessor().userProfile(forID: target))
			.flatMap { (result) -> Future<(UserProfile, (target: ID, delta: Int))> in
				let (saveRequest, userProfile) = result;
				var modified = false;
				var saveModification: (target: ID, delta: Int) = (target: saveRequest.element, delta: 0);
				switch saveRequest.operation {
				case .insert:
					if !userProfile.savedItems.contains(likeRequest.element) {
						userProfile.likedItems.append(likeRequest.element);
						modified = true;
						saveModification.delta = 1;
					}
				case .remove:
					if let index = userProfile.savedItems.index(of: likeRequest.element) {
						userProfile.savedItems.remove(at: index);
						modified = true;
						saveModification.delta = -1;
					}
				}
				if modified {
					return try req.profileAccessor().update(profile: userProfile).and(result: likeModification);
				} else {
					return req.future(userProfile).and(result: likeModification);
				}
			}.flatMap { (result) -> Future<(Item, Int)> in
				let (_, (target, delta)) = result;
				return try req.itemAccessor().item(forID: target).and(result: delta);
			}.flatMap({ (result) -> EventLoopFuture<Item> in
				let (item, delta) = result;
				if delta != 0 {
					if delta > 0 {
						item.incrementSaves();
					} else if delta < 0 {
						item.decrementSaves();
					}
					return try req.itemAccessor().update(item: item);
				} else {
					return req.future(item);
				}
			}).map({ (item) -> (VoidResponse) in
				return VoidResponse();
			})
	}
	
}
