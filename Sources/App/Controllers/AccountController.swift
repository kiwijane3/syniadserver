//
//  AccountController.swift
//  App
//
//  Created by Jane Fraser on 19/01/20.
//

import Foundation
import Vapor
import SyniadShared
import Crypto
import JWT

public class AccountController {
	
	public func register(to router: Router) {
		router.post("/createAccount", use: createAccount);
		router.post("/login", use: login);
		router.post("/validate", use: validateToken);
	}
	
	// Creates a new account and returns a jwt for the new account.
	public func createAccount(_ req: Request) throws -> Future<LoginInfo> {
		return try req.content.decode(AccountCreationRequest.self).flatMap { (creationRequest) -> Future<LoginData> in
			// Create a cryptographically secure random secret.
			let secret = try CryptoRandom().generateData(count: 8);
			// Combine the password and the secret and store the result. Then, when a login attempt is received, we can repeat this process with the provided password and compare the result with the result we get here; If it is the same, the password is correct, as the combination is a consistent function of inputs. This method allows us to check passwords without needing to store them, which would be a security risk.
			let result = try HMAC.SHA256.authenticate(creationRequest.password, key: secret);
			// Now that we have done that, we can create a blank user profile with the desired username
			let profile = UserProfile(withUsername: creationRequest.username);
			try req.profileAccessor().insert(profile: profile);
			// We can now generate the account record.
			let loginData = LoginData(email: creationRequest.email, secret: secret, result: result, userProfile: profile.id);
			return try req.loginAccessor().insert(loginData: loginData)
		}.map { loginData in
			let profile = loginData.userProfile;
			let token = try req.jwtService().generate(forUser: loginData.userProfile);
			return LoginInfo(profile: profile, token: token);
		}
	}
	
	// Processes a login request and returns a jwt to the account if it exists and the credentials are correct., otherwise throws an error..
	internal func login(_ req: Request) throws -> Future<LoginInfo> {
		return try req.content.decode(LoginRequest.self).flatMap { loginRequest -> EventLoopFuture<(LoginData, String)> in
			return try req.loginAccessor().loginData(forEmail: loginRequest.email).and(result: loginRequest.password);
		}.map { (result) -> LoginInfo in
			
			let (loginData, password) = result;
			
			// Combine the password with the secret and check against the intial result to verify the password.
			let calculatedResult = try HMAC.SHA256.authenticate(password, key: loginData.secret);
			if calculatedResult != loginData.result {
				throw AccessError.wrongPassword;
			}
			let profile = loginData.userProfile;
			let token = try req.jwtService().generate(forUser: loginData.userProfile)
			return LoginInfo(profile: profile, token: token)
		}
	}
	
	internal func validateToken(_ req: Request) throws -> Future<VoidResponse> {
		return try req.content.decode(LoginInfo.self).map { loginInfo -> VoidResponse in
			guard try req.jwtService().verifyAuthorisation(for: loginInfo.token, toProfile: loginInfo.profile) else {
				throw AccessError.invalidToken;
			}
			return VoidResponse()
		}
	}
	
}
