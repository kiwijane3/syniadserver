//
//  Error.swift
//  App
//
//  Created by Jane Fraser on 20/01/20.
//

import Foundation
import Vapor

public enum AccessError: AbortError {
	
	case wrongPassword
	case noSuchAccount
	case invalidToken
	case unauthorised
	
	public var status: HTTPStatus {
		get {
			return .unauthorized;
		}
	}
	
	public var identifier: String {
		get {
			switch self {
			case .wrongPassword:
				return "wrongPassword";
			case .noSuchAccount:
				return "NoSuchAccount";
			case .invalidToken:
				return "invalidToken";
			case .unauthorised:
				return "unauthorised";
			}
		}
	}
	
	public var reason: String {
		get {
			switch self {
			case .wrongPassword:
				return "The password was incorrect";
			case .noSuchAccount:
				return "Could not find account information for the given login credentials";
			case .invalidToken:
				return "The token provided was not valid; i.e, could not verify, wrong type";
			case .unauthorised:
				return "User was not authorised to perform requested action";
			}
		}
	}
	
}

public struct DatabaseError: AbortError {
	
	public var status: HTTPResponseStatus {
		get {
			return .internalServerError;
		}
	}
	
	public var identifier: String {
		get {
			return "databaseError";
		}
	}
	
	public var reason: String;
	
	public init(reason: String?) {
		self.reason = reason ?? "Unspecified";
	}
	
}

public enum ResourceError: AbortError {
	
	case nonexistent
	case invalid
	case incompatibleType
	// Indicates the server attempted to access an cryptographic key, but no file was at the target url.
	case keyNotFound
	
	public var status: HTTPResponseStatus {
		get {
			switch self {
			case .nonexistent:
				return .notFound;
			case .invalid:
				return .badRequest;
			case .incompatibleType:
				return .unsupportedMediaType;
			case .keyNotFound:
				return .internalServerError;
			}
		}
	}
	
	public var identifier: String {
		switch self {
		case .nonexistent:
			return "nonexistent";
		case .invalid:
			return "invalid";
		case .incompatibleType:
			return "incompatibleFormat";
		case .keyNotFound:
			return "keyNotFound";
		}
	}
	
	public var reason: String {
		get {
			switch self {
			case .nonexistent:
				return "Attempted to access resource that does not exist";
			case .invalid:
				return "Invalid resource creation or modification requested";
			case .incompatibleType:
				return "Data provided was of the incorrect type";
			case .keyNotFound:
				return "Could not find cryptographic key for signing operation";
			}
		}
	}
	
}
