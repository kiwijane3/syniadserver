//
//  JwtService.swift
//  App
//
//  Created by Jane Fraser on 17/01/20.
//

import Foundation
import Vapor
import JWT
import SyniadShared
import Crypto

let jwtExpirationTime: TimeInterval = 604800;
let issuer = "luoja-syniad-server";


// Creates, verifies, and extracts users from JWTs.
public class JwtService: Service {
	
	struct Payload: JWTPayload {
		
		let iss: IssuerClaim;
		
		// The ID of the user profile this token authorizes the user to access.
		let sub: SubjectClaim;
		
		let exp: ExpirationClaim;
		
		let iat: IssuedAtClaim;
		
		public init(forUser userID: ID) {
			self.iss = IssuerClaim.init(value: issuer);
			self.sub = SubjectClaim(value: userID);
			let now = Date();
			self.iat = IssuedAtClaim(value: now);
			self.exp = ExpirationClaim(value: now.addingTimeInterval(jwtExpirationTime));
		}
		
		public func verify(using signer: JWTSigner) throws {
			if self.iss.value != issuer {
				throw JWTError(identifier: "IssError", reason: "Wrong issuer");
			}
			try self.exp.verifyNotExpired();
		}
		
	}
	
	public let privateKey: RSAKey;
	
	public init() throws {
		do {
			let data = try Data(contentsOf: URL(fileURLWithPath: "\(NSHomeDirectory())/.syniad/key.pem"));
			privateKey = try RSAKey.private(pem: data);
		} catch {
			print(error);
			throw ResourceError.keyNotFound;
		}
	}
	
	public func generate(forUser userID: ID) throws -> String {
		let payload = Payload(forUser: userID);
		let data = try JWT(payload: payload).sign(using: .rs256(key: privateKey))
		return String(data: data, encoding: .utf8) ?? "";
	}
	
	public func verifyUser(for request: Request) -> ID? {
		guard let bearer = request.http.headers.bearerAuthorization else {
			return nil;
		}
		return verifyUser(for: bearer.token);
	}
	
	public func verifyUser(for token: String) -> ID? {
		let jwt = try? JWT<Payload>.init(from: token, verifiedUsing: .rs256(key: privateKey));
		return jwt?.payload.sub.value;
	}
	
	public func verifyAuthorisation(for request: Request, toProfile profileID: ID) -> Bool {
		guard let bearer = request.http.headers.bearerAuthorization else {
			return false;
		}
		return verifyAuthorisation(for: bearer.token, toProfile: profileID);
	}
	
	public func verifyAuthorisation(for token: String, toProfile profileID: ID) -> Bool {
		if let tokenID = verifyUser(for: token), tokenID == profileID {
			return true;
		} else {
			return false;
		}
	}
	
}

