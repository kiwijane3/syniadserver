//
//  Query.swift
//  App
//
//  Created by Jane Fraser on 24/01/20.
//

import Foundation
import MongoKitten

public extension Query {
	
	static func textSearch(for term: String) -> Query {
		return .custom(["$text": ["$search": term]]);
	}

}
