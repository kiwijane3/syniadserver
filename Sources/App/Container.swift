//
//  Container.swift
//  App
//
//  Created by Jane Fraser on 20/01/20.
//

import Foundation
import Vapor

// Convenience extensions for accessing services.
public extension Container {
	
	func jwtService() throws -> JwtService {
		return try self.make(JwtService.self);
	}
	
	func dataAccessor() throws -> DataAccessor {
		return try self.make(DataAccessor.self);
	}
	
	func loginAccessor() throws -> LoginAccessor {
		return try self.make(LoginAccessor.self);
	}
	
	func profileAccessor() throws -> ProfileAccessor {
		return try self.make(ProfileAccessor.self);
	}
	
	func itemAccessor() throws -> ItemAccessor {
		return try self.make(ItemAccessor.self);
	}
	
	func reviewAccessor() throws -> ReviewAccessor {
		return try self.make(ReviewAccessor.self);
	}
	
	func projectAccessor() throws -> ProjectAccessor {
		return try self.make(ProjectAccessor.self);
	}
	
	func feedAccessor() throws -> FeedAccessor {
		return try self.make(FeedAccessor.self);
	}
	
}
