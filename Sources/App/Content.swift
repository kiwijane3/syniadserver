//
//  Content.swift
//  App
//
//  Created by Jane Fraser on 20/01/20.
//

import Foundation
import SyniadShared
import Vapor

extension UserProfile: Vapor.Content {}

extension Item: Vapor.Content {}

extension ItemReview: Vapor.Content {}

extension DataContainer: Vapor.Content {}

extension Project: Vapor.Content {}

extension LoginInfo: Vapor.Content {}

extension Post: Vapor.Content {}

public struct VoidResponse: Vapor.Content {}
