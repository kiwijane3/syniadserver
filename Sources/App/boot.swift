import Vapor

/// Called after your application has initialized.
public func boot(_ app: Application) throws {
    // Your code here
	debugPrint("Boot");
	// Ensure all indexes are created.
	try Future.andAll([
		try app.itemAccessor().createIndexes(),
		try app.loginAccessor().createIndexes(),
		try app.reviewAccessor().createIndexes(),
		try app.projectAccessor().createIndexes(),
		try app.feedAccessor().createIndexes()
	], eventLoop: app.eventLoop).wait();
}
