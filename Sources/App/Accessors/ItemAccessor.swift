//
//  ItemAccessor.swift
//  App
//
//  Created by Jane Fraser on 21/01/20.
//

import Foundation
import Vapor
import MongoKitten
import SyniadShared

var itemCollectionIdentifier = "items";

public class ItemAccessor: Service {
	
	public var database: MongoKitten.Database;
	
	public var collection: Collection {
		get {
			return database[itemCollectionIdentifier];
		}
	}
	
	public init(database: MongoKitten.Database) {
		self.database = database;
		// Create a text index on the title and description
	}
	
	public func createIndexes() -> Future<Void> {
		let futures = [
			collection.indexes.createCompound(named: "text", keys: [ "title": .text, "description": .text]),
			collection.indexes.create(Index(named: "score", keys: ["score": .ascending])),
			collection.indexes.create(Index(named: "style", keys: ["style": .ascending])),
			collection.indexes.create(Index(named: "category", keys: ["category": .ascending])),
			collection.indexes.create(Index(named: "tags", keys: ["tags": .ascending])),
			collection.indexes.create(Index(named: "publisher", keys: ["publisherID": .ascending]))
		]
		return Future.andAll(futures, eventLoop: database.eventLoop);
	}
	
	public func insert(item: Item) throws -> Future<Item> {
		return try collection.insert(item).map { (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.errorMessage);
			}
			return item;
		}
	}
	
	public func update(item: Item) throws -> Future<Item> {
		return try collection.update(where: "_id" == item.id, to: item).map { reply in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.localizedDescription);
			}
			return item;
		}
	}
	
	public func item(forID id: ID) throws -> Future<Item> {
		return collection.findOne("_id" == id, as: Item.self).unwrap(or: ResourceError.nonexistent);
	}
	
	public func items(for term: String? = nil, in category: SyniadShared.Category? = nil, withStyle style: Style? = nil, tagged tags: [Tag]? = nil, by publisher: ID? = nil, skip: Int? = nil, limit: Int? = nil) throws -> Future<[Item]> {
		var queries = [Query]();
		if let term = term {
			queries.append(Query.textSearch(for: term));
		}
		if let category = category {
			queries.append("category" == category.makePrimitive());
		}
		if let style = style {
			queries.append("style" == style);
		}
		if let tags = tags {
			queries.append(contentsOf: tags.map { (tag) in
				return Query.containsElement(field: "tags", match: ["$eq": tag]);
			})
		}
		if let publisher = publisher {
			queries.append("publisherID" == publisher)
		}
		// Sort based on the calculated score, which is based on reviews.
		var sort: Sort = ["score": .descending];
		// If a search term has been specified, prioritise sort based on text score.
		if term != nil {
			sort = sort + ["searchScore": .textScore];
		}
		var cursor = collection.find(.and(queries))
		if term != nil {
			cursor = cursor.project(["searchScore": [ "$meta": "textScore"]]);
		}
		return try cursor.limit(limit ?? 20).skip(skip ?? 0).sort(sort).getAllResults().map({ (documents) -> [Item] in
			let decoder = BSONDecoder();
			return try documents.map { (document) -> Item in
				return try decoder.decode(Item.self, from: document);
			}
		});
	}
	
}
