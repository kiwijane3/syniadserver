//
//  ReviewAccessor.swift
//  App
//
//  Created by Jane Fraser on 23/01/20.
//

import Foundation
import Vapor
import SyniadShared
import MongoKitten

var reviewCollectionIdentifer = "reviews";

public class ReviewAccessor: Service {
	
	public var database: MongoKitten.Database;
	
	private var collection: Collection {
		get {
			return database[reviewCollectionIdentifer];
		}
	}
	
	public init(database: MongoKitten.Database) {
		self.database = database;
	}

	public func createIndexes() -> Future<Void> {
		return collection.indexes.create(Index(named: "itemID", keys: ["itemID": .ascending]));
	}
	
	public func insert(review: ItemReview) throws -> Future<ItemReview> {
		return try collection.insert(review).map({ (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.errorMessage);
			}
			return review;
		})
	}
	
	public func update(review: ItemReview) throws -> Future<ItemReview>{
		return try collection.update(where: "_id" == review.id, to: review).map({ (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.localizedDescription);
			}
			return review;
		})
	}
	
	public func review(forID id: ID) throws -> Future<ItemReview> {
		return try collection.findOne("_id" == id, as: ItemReview.self).unwrap(or: ResourceError.nonexistent);
	}
	
	public func review(fromUser userID: ID, onItem itemID: ID) throws -> Future<ItemReview> {
		return try collection.findOne("userID" == userID && "itemID" == itemID, as: ItemReview.self).unwrap(or: ResourceError.nonexistent);
	}
	
	public func reviews(forItem itemID: ID, skip: Int? = nil, size: Int? = nil) throws -> Future<[ItemReview]> {
		return try collection.find("itemID" == itemID)
			.sort(["postTime": .descending])
			.skip(skip ?? 0)
			.limit(size ?? 20)
			.getAllResults().map{ (documents) in
				let decoder = BSONDecoder();
				return try documents.map { (document) in
					try decoder.decode(ItemReview.self, from: document);
				}
			}
	}
	
}
