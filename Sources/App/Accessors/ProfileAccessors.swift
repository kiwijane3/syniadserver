//
//  ProfileAccessors.swift
//  App
//
//  Created by Jane Fraser on 19/01/20.
//

import Foundation
import SyniadShared
import Vapor
import MongoKitten

let profileCollectionIdentifier = "profile";

// Abstracts database interactions related to public user profiles.
public class ProfileAccessor: Service {
	
	public var database: MongoKitten.Database;
	
	public var collection: Collection {
		get {
			return database[profileCollectionIdentifier];
		}
	}
	
	public init(database: MongoKitten.Database) {
		self.database = database;
	}
	
	public func insert(profile: UserProfile) throws -> Future<UserProfile> {
		try collection.insert(profile).map{ (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.errorMessage);
			}
			return profile;
		}
	}
	
	// Updates an existing document to a new version.
	public func update(profile: UserProfile) throws -> Future<UserProfile> {
		try collection.update(where: "_id" == profile.id, to: profile).map({ (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.localizedDescription);
			}
			return profile;
		})
	}
	
	public func userProfile(forID id: ID) throws -> Future<UserProfile> {
		return collection.findOne("_id" == id, as: UserProfile.self).unwrap(or: ResourceError.nonexistent);
	}
	
}
