//
//  LoginAccessor.swift
//  App
//
//  Created by Jane Fraser on 17/01/20.
//

import Foundation
import Vapor
import MongoKitten
import SyniadShared

let loginCollectionIdentifier = "login"

// Abstracts database interactions for login/account information.
public class LoginAccessor: Service {
	
	public var database: MongoKitten.Database;
	
	public var collection: MongoKitten.Collection {
		get {
			return database["loginCollectionIdentifier"];
		}
	}
	
	public init(database: MongoKitten.Database) {
		self.database = database;
	}
	
	public func createIndexes() -> Future<Void> {
		return collection.indexes.create(Index(named: "email", keys: ["email": .ascending]));
	}
	
	public func loginData(forEmail email: String) -> Future<LoginData> {
		return collection.findOne("email" == email, as: LoginData.self).unwrap(or: ResourceError.nonexistent);
	}
	
	public func insert(loginData: LoginData) throws -> Future<LoginData> {
		return try collection.insert(loginData).map({ (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.errorMessage);
			}
			return loginData;
		})
	}
	
}
