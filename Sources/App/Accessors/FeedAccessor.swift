//
//  PostAccessor.swift
//  App
//
//  Created by Jane Fraser on 28/01/20.
//

import Foundation
import MongoKitten
import Vapor
import SyniadShared

let feedCollectionIdentifier = "posts";

public class FeedAccessor: Service {
	
	public var database: MongoKitten.Database;
	
	public var collection: Collection {
		get {
			return database[feedCollectionIdentifier];
		}
	}
	
	public init(database: MongoKitten.Database) {
		self.database = database;
	}
	
	public func createIndexes() -> Future<Void> {
		return try Future.andAll([
			collection.indexes.create(Index(named: "user", keys: ["user": .ascending])),
			collection.indexes.create(Index(named: "date", keys: ["date": .descending]))
		], eventLoop: database.eventLoop);
	}
	
	public func insert(post: Post) throws -> Future<Post> {
		return try collection.insert(post).map({ (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.localizedDescription);
			}
			return post;
		})
	}
	
	public func post(forID id: ID) throws -> Future<Post> {
		return try collection.findOne("_id" == id, as: Post.self).unwrap(or: ResourceError.nonexistent);
	}
	
	public func posts(fromUser user: ID, skip: Int? = nil, limit: Int? = nil) throws -> Future<[Post]> {
		return try collection.find("user" == user).skip(skip ?? 0).limit(limit ?? 20).getAllResults().map({ (documents) in
			let decoder = BSONDecoder();
			return try documents.map({ (document) in
				return try decoder.decode(Post.self, from: document);
			})
		})
	}
	
	// TODO: Implement filtering for content types.
	public func posts(fromUsers users: [ID], skip: Int? = nil, limit: Int? = nil) throws -> Future<[Post]> {
		return try collection.find(.in(field: "user", in: users)).skip(skip ?? 0).limit(limit ?? 20).getAllResults().map({ (documents) in
			let decoder = BSONDecoder();
			return try documents.map({ (document) in
				return try decoder.decode(Post.self, from: document);
			})
		})
	}
	
}
