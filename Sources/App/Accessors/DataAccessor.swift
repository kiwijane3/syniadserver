//
//  DataAccessor.swift
//  App
//
//  Created by Jane Fraser on 20/01/20.
//

import Foundation
import Vapor
import MongoKitten
import SyniadShared

let dataCollectionIdentifier = "data";

// Abstracts database interactions with
public class DataAccessor: Service {
	
	public var database: MongoKitten.Database;
	
	private var collection: Collection {
		get {
			return database[dataCollectionIdentifier]
		}
	}
	
	public init(database: MongoKitten.Database) {
		self.database = database;
	}
	
	public func store(data: Data, as dataType: DataType, withID id: ID? = nil) throws -> Future<ID> {
		let container = DataContainer(data: data, type: dataType, id: id);
		return try collection.insert(container).map({ (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.errorMessage);
			}
			return container.id;
		});
	}
	
	public func store(contentsOf data: [Data], as dataType: DataType) throws -> Future<[ID]> {
		let containers = data.map { (data) in
			return DataContainer(data: data, type: dataType);
		}
		return try collection.insert(contentOf: containers).map({ (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.errorMessage);
			}
			return containers.map { (container) in
				return container.id;
			}
		})
	}
	
	public func store(contentsOf dataRequests: [DataRequest]) throws -> Future<[ID]> {
		let containers = dataRequests.map { dataRequest in
			return DataContainer(data: dataRequest.data, type: dataRequest.dataType, id: dataRequest.id);
		}
		return try collection.insert(contentOf: containers).map({ (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.errorMessage);
			}
			return containers.map { (container) in
				return container.id;
			}
		})
	}
	
	public func retrieve(_ id: ID) -> Future<DataContainer> {
		collection.findOne("_id" == id, as: DataContainer.self).unwrap(or: ResourceError.nonexistent);
	}
	
}
