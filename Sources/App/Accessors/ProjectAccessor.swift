//
//  ProjectAccessor.swift
//  App
//
//  Created by Jane Fraser on 27/01/20.
//

import Foundation
import Vapor
import MongoKitten
import SyniadShared

public let projectCollectionIdentifier = "projects";

public class ProjectAccessor: Service {
	
	public var database: MongoKitten.Database;
	
	public var collection: Collection {
		get {
			return database[projectCollectionIdentifier];
		}
	}
	
	public init(database: MongoKitten.Database) {
		self.database = database;
	}
	
	public func createIndexes() -> Future<Void> {
		return Future.andAll([
			collection.indexes.create(Index(named: "user", keys: ["user": .ascending])),
			collection.indexes.create(Index(named: "item", keys: ["item": .ascending])),
			collection.indexes.create(Index(named: "updateDate", keys: ["updateDate": .ascending]))
		], eventLoop: database.eventLoop);
	}
	
	public func insert(project: Project) throws -> Future<Project> {
		try self.collection.insert(project).map { reply in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.errorMessage ?? "Unspecified");
			}
			return project;
		}
	}
	
	public func update(project: Project) throws -> Future<Project> {
		try self.collection.update(where: "_id" == project._id, to: project).map { reply in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.localizedDescription);
			}
			return project;
		}
	}
	
	public func project(forID id: ID) throws -> Future<Project> {
		self.collection.findOne("_id" == id, as: Project.self).unwrap(or: ResourceError.nonexistent);
	}
	
	public func projects(from userID: ID, skip: Int? = nil, limit: Int? = nil) throws -> Future<[Project]> {
		self.collection.find("user" == userID).skip(skip ?? 0).limit(limit ?? 20).sort(["updateDate": .descending]).getAllResults().map { (documents)  in
			let decoder = BSONDecoder();
			return try documents.map { document in
				return try decoder.decode(Project.self, from: document);
			}
		}
		// TODO: Insert other sort methods once other sortable fields are implemented
	}
	
	public func projects(forItem itemID: ID, skip: Int? = nil, limit: Int? = nil) throws -> Future<[Project]> {
		self.collection.find("item" == itemID).skip(skip ?? 0).limit(limit ?? 20).sort(["updateDate": .descending]).getAllResults().map { (documents) in
			let decoder = BSONDecoder();
			return try documents.map { document in
				return try decoder.decode(Project.self, from: document);
			}
		}
	}
	
}
