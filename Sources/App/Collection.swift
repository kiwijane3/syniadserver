//
//  Collection.swift
//  App
//
//  Created by Jane Fraser on 19/01/20.
//

import Foundation
import MongoKitten
import Vapor

public extension Collection {
	
	func insert(_ codable: Encodable) throws -> Future<InsertReply> {
		let document = try BSONEncoder().encode(codable);
		return try self.insert(document);
	}
	
	func insert(contentOf codables: [Encodable]) throws -> Future<InsertReply> {
		let encoder = BSONEncoder();
		let documents = try codables.map { (codable) in
			return try encoder.encode(codable);
		}
		return self.insert(documents: documents);
	}
	
	func update(where query: Query, to codable: Encodable) throws -> Future<UpdateReply> {
		let document = try BSONEncoder().encode(codable);
		return try self.update(where: query, to: codable);
	}
	
}

public extension Future where T == InsertReply {
	
	func succeedOrThrow() -> Future<Void> {
		return self.map { (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.errorMessage);
			}
		}
	}
	
}

public extension Future where T == UpdateReply {
	
	func succeedOrThrow() -> Future<Void> {
		return self.map { (reply) in
			guard reply.isSuccessful else {
				throw DatabaseError(reason: reply.localizedDescription);
			}
		}
	}
	
}
